//
//  Extensions.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 18/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation
import UIKit

extension Date {

    static var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        return formatter
    }

    var toString: String {
        return Date.formatter.string(from: self)
    }

    static func fromString(string: String) -> Date {
        return formatter.date(from: string)!
    }
}

extension UIScrollView {

    func addPullToRefresh(target: Any, action: Selector) {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(target, action: action, for: .valueChanged)
        refreshControl?.tintColor = UIColor.black
    }

    func removePullToRefresh() {
        refreshControl = nil
    }
}

extension UIViewController {

    func showErrorAlert(message: String?) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        self.present(alertController, animated: true, completion: nil)
    }

    func showDestructiveAlert(message: String, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in completion() }))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension Bool {
    mutating func inverse() {
        self = !self
    }
}
