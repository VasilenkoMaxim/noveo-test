//
//  Feed+CoreData.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 17/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension Feed {

    // Use with caution! The channel must be from the moc transmitted to this function.
    static func getAll<T: Feed>(in moc: NSManagedObjectContext, for channel: Channel) throws -> [T] {
        guard let channelUrl = channel.url, !channelUrl.isEmpty else {
            throw ChannelError.channelEntityWithoutUrl
        }
        let entityName = String(describing: self)
        let request: NSFetchRequest<T> = NSFetchRequest<T>(entityName: entityName)
        let predicate = NSPredicate(format: "\(#keyPath(Feed.channel.url)) == %@", channelUrl)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(Feed.pubDate), ascending: false)]
        return try moc.fetch(request)
    }

    static func getFeedsAccordingToOptions<T: Feed>(in moc: NSManagedObjectContext) throws -> [T] {
        let entityName = String(describing: self)
        let request: NSFetchRequest<T> = NSFetchRequest<T>(entityName: entityName)
        guard let predicate = Options.predicateForFetchFeed(in: moc) else { return [] }
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        if let sortDescriptors = Options.sortDescriptorsForFetchFeed() {
            request.sortDescriptors = sortDescriptors
        }
        return try moc.fetch(request)
    }

    static func cleanAll(in moc: NSManagedObjectContext) throws {
        let entityName = String(describing: self)
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        try moc.persistentStoreCoordinator?.execute(deleteRequest, with: moc)
        let channels = try? Channel.getAll(in: moc)
        channels?.forEach({ $0.lastUpdateAt = nil })
    }

    func img() -> UIImage? {
        guard let image = image else {
            return nil
        }
        return UIImage(data: image)
    }

    func setProperties(channel: Channel? = nil, url: String? = nil, title: String? = nil,
                       descriptionText: String? = nil, descriptionString: String? = nil,
                       image: UIImage? = nil, pubDate: Date? = nil) {
        if channel != nil { self.channel = channel }
        if url != nil { self.url = url }
        if title != nil { self.title = title }
        if descriptionText != nil { self.descriptionText = descriptionText }
        if descriptionString != nil { self.descriptionString = descriptionString }
        if image != nil { self.image = image?.pngData() }
        if pubDate != nil { self.pubDate = pubDate }
    }
}
