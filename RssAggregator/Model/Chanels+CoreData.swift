//
//  Chanels+CoreData.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 16/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension Channel {

    static func getAll<T: Channel>(in moc: NSManagedObjectContext) throws -> [T] {
        let entityName = String(describing: self)
        let request: NSFetchRequest<T> = NSFetchRequest<T>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(Channel.addedAt), ascending: false)]
        return try moc.fetch(request)
    }

    static func getVisible<T: Channel>(in moc: NSManagedObjectContext) throws -> [T] {
        let entityName = String(describing: self)
        let request: NSFetchRequest<T> = NSFetchRequest<T>(entityName: entityName)
        let predicate = NSPredicate(format: "\(#keyPath(Channel.isVisible)) == %@", argumentArray: [true])
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(Channel.addedAt), ascending: false)]
        return try moc.fetch(request)
    }

    func img() -> UIImage? {
        guard let icon = icon else {
            return nil
        }
        return UIImage(data: icon)
    }

    func setProperties(name: String? = nil, url: String? = nil, addedAt: Date? = nil,
                       title: String? = nil, icon: UIImage? = nil, lastUpdateAt: Date? = nil) {
        if name != nil { self.name = name }
        if url != nil { self.url = url }
        if addedAt != nil { self.addedAt = addedAt }
        if title != nil { self.title = title }
        if icon != nil { self.icon = icon?.pngData() }
        if lastUpdateAt != nil { self.lastUpdateAt = lastUpdateAt }
    }
}
