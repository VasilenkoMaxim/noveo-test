//
//  FeedManager.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 17/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

let sleepTimeMiliseconds = 100

import Foundation
import CoreData
import FeedKit
import UIKit
import SwiftSoup

class FeedManager {

    private class ThreadInspector {
        var error: Error?
        var isTaskEnd = false
        let threadNumber: Int

        init(threadNumber: Int) {
            self.threadNumber = threadNumber
        }
    }

    static let shared = FeedManager()

    func loadFeedsFromInternet(to container: NSPersistentContainer, completion: (([Error]?) -> Void)? = nil) {
        container.performBackgroundTask { [weak self] moc in
            guard let channels = try? Channel.getVisible(in: moc) else {
                DispatchQueue.main.async {
                    completion?([ChannelError.loadFromCoreData])
                }
                return
            }
            var threadInspectors: [ThreadInspector] = []
            for index in 0..<channels.count {
                threadInspectors.append(ThreadInspector(threadNumber: index))
            }
            threadInspectors.forEach { threadInspector in
                self?.loadFeedsFromInternet(to: container, for: channels[threadInspector.threadNumber],
                                            with: threadInspector)
            }
            while !threadInspectors.allSatisfy({ $0.isTaskEnd }) {
                usleep(UInt32(sleepTimeMiliseconds))
            }
            DispatchQueue.main.async {
                completion?(nil)
            }
        }
    }

    private func loadFeedsFromInternet(to container: NSPersistentContainer, for channel: Channel,
                                       with threadInspector: ThreadInspector) {
        let channelUri = channel.objectID.uriRepresentation()
        container.performBackgroundTask { [weak self] moc in
            guard let channelID = moc.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: channelUri),
                let channel = moc.object(with: channelID) as? Channel,
                let channelUrlString = channel.url,
                let channelUrl = URL(string: channelUrlString),
                let feeds = try? Feed.getAll(in: moc, for: channel) else {
                threadInspector.isTaskEnd = true
                return
            }
            let parser = FeedParser(URL: channelUrl)
            let result = parser.parse()
            guard let rssFeeds = result.rssFeed else {
                threadInspector.isTaskEnd = true
                return
            }
            channel.setProperties(title: rssFeeds.title, icon: self?.loadImageByUrl(urlString: rssFeeds.image?.url),
                                  lastUpdateAt: Date())
            rssFeeds.items?.forEach { item in
                guard !feeds.contains(where: { $0.url == item.link}) else {
                    return
                }
                self?.addFeedToCoreData(item: item, channel: channel, moc: moc)
            }
            try? moc.save()
            threadInspector.isTaskEnd = true
            return
        }
    }

    private func loadImageByUrl(urlString: String?) -> UIImage? {
        guard let urlString = urlString,
            let url = URL(string: urlString),
            let imageData = try? Data(contentsOf: url),
            let image = UIImage(data: imageData) else {
            return nil
        }
        return image
    }

    private func addFeedToCoreData(item: RSSFeedItem, channel: Channel, moc: NSManagedObjectContext) {
        guard let feedUrl = item.link, let feedTittle = item.title else {
            return
        }
        let newFeed = Feed(context: moc)
        var feedImage = loadImageByUrl(urlString: item.media?.mediaThumbnails?.first?.attributes?.url)
        if feedImage == nil {
            feedImage = tryLoadImageFromDescription(description: item.description)
        }
        let descriptionText = try? (try? SwiftSoup.parseBodyFragment(item.description ?? ""))?.text()
        newFeed.setProperties(channel: channel, url: feedUrl, title: feedTittle,
                              descriptionText: descriptionText, descriptionString: item.description,
                              image: feedImage, pubDate: item.pubDate)
        channel.feeds?.adding(newFeed)
    }

    private func tryLoadImageFromDescription(description: String?) -> UIImage? {
        guard let description = description else {
            return nil
        }
        let doc = try? SwiftSoup.parseBodyFragment(description)
        let srcs = try? doc?.select("img[src]")
        let srcsStringArray = srcs?.array().map { try? $0.attr("src").description }
        guard let urlImage = srcsStringArray?.first,
            let image = loadImageByUrl(urlString: urlImage)else {
            return nil
        }
        return resizeImage(image: image, toTheSize: CGSize(width: 70, height: 70))
    }

    private func resizeImage(image: UIImage, toTheSize size: CGSize) -> UIImage {
        let scale = CGFloat(max(size.width/image.size.width,
                                size.height/image.size.height))
        let width: CGFloat = image.size.width * scale
        let height: CGFloat = image.size.height * scale

        let rect: CGRect = CGRect(x: 0, y: 0, width: width, height: height)

        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
