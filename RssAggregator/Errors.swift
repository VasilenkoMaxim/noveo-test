//
//  Errors.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 16/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation

enum ChannelError: Error {
    case channelExist
    case nameNotSet
    case urlNotSet
    case channelEntityWithoutUrl
    case loadFromCoreData
}

extension ChannelError: LocalizedError {

    public var errorDescription: String? {
        switch self {
        case .channelExist:
            return "Channel with this URL is exist"
        case .nameNotSet:
            return "Channel Name is not set"
        case .urlNotSet:
            return "Channel URL is not set"
        case .channelEntityWithoutUrl:
            return nil
        case .loadFromCoreData:
            return nil
        }
    }
}
