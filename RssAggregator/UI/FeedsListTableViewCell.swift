//
//  FeedsListTableViewCell.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 18/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit

class FeedsListTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pubDateLabel: UILabel!
    @IBOutlet weak var channelIconView: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    private var isFavourite = false
    private var tapFavouriteButtonAction: (() -> Void)?

    @IBAction func changeFavouriteStatus(_ sender: Any) {
        isFavourite = !isFavourite
        if isFavourite {
            favouriteButton.setImage(#imageLiteral(resourceName: "FavouriteSelect"), for: .normal)
        } else {
            favouriteButton.setImage(#imageLiteral(resourceName: "FavouriteUnselect"), for: .normal)
        }
        favouriteButton.imageView?.setNeedsDisplay()
        tapFavouriteButtonAction?()
    }

    func configure(icon: UIImage? = nil, title: String? = nil,
                   description: String? = nil, pubDate: String? = nil,
                   channelIcon: UIImage? = nil, isFavourite: Bool? = nil,
                   didTapFavouriteButton action: (() -> Void)? = nil) {
        iconView.image = icon
        titleLabel.text = title
        descriptionLabel.text = description
        pubDateLabel.text = pubDate
        channelIconView.image = channelIcon
        self.isFavourite = isFavourite ?? false
        if self.isFavourite {
            favouriteButton.setImage(#imageLiteral(resourceName: "FavouriteSelect"), for: .normal)
        } else {
            favouriteButton.setImage(#imageLiteral(resourceName: "FavouriteUnselect"), for: .normal)
        }
        self.tapFavouriteButtonAction = action
    }
}
