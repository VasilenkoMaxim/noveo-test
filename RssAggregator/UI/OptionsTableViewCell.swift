//
//  OptionsTableViewCell.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 19/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation
import UIKit

class OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var optionDescription: UILabel!

    private var action: ((_ isOn: Bool) -> Void)?

    @IBAction func switchChangeValue(_ sender: UISwitch) {
        action?(sender.isOn)
    }

    func configure(description: String, state: Bool? = nil, action: ((Bool) -> Void)? = nil) {
        optionDescription.text = description
        if let state = state {
            switcher.isOn = state
        } else {
            switcher.isHidden = true
        }
        self.action = action
    }
}
