//
//  FeedViewController.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 18/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit
import WebKit

class FeedViewController: UIViewController, UIWebViewDelegate {

    deinit {
        delegate?.feedViewWillClose(isFavourite: isFavourite, indexPath: indexPath)
    }

    @IBOutlet weak var webView: UIWebView!
    weak var delegate: BackToFeedsListFromFeedViewDelegate?
    private var url: URL?
    private var lastRequest: URLRequest?
    private var favouriteBarButton: UIBarButtonItem?
    private var isFavourite = false
    private var indexPath: IndexPath?

    func configure(with urlString: String?, isFavourite: Bool, indexPath: IndexPath) {
        guard let urlString = urlString else {
            return
        }
        self.isFavourite = isFavourite
        self.indexPath = indexPath
        url = URL(string: urlString)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = url else {
            return
        }
        favouriteBarButton = UIBarButtonItem(image: isFavourite ?  #imageLiteral(resourceName: "FavouriteSelect") : #imageLiteral(resourceName: "FavouriteUnselect"), style: .plain, target: self,
                                             action: #selector(tapToFavoritButton))
        self.navigationItem.setRightBarButtonItems([favouriteBarButton ?? UIBarButtonItem()], animated: true)
        webView.scrollView.addPullToRefresh(target: self, action: #selector(refresh(sender:)))
        webView.scrollView.refreshControl?.beginRefreshing()
        webView.delegate = self
        webView.loadRequest(URLRequest(url: url))
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.scrollView.refreshControl?.endRefreshing()
        lastRequest = webView.request
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webView.scrollView.refreshControl?.endRefreshing()
        // I know its crutch)))!
        if (error as NSError).domain == "NSURLErrorDomain" {
            return
        }
        showErrorAlert(message: error.localizedDescription)
    }

    @objc private func refresh(sender: UIRefreshControl) {
        guard let refcon = webView.scrollView.refreshControl, refcon.isRefreshing else {
            return
        }
        if webView.request?.url?.absoluteString.isEmpty ?? true {
            guard let url = url else { return }
            webView.loadRequest(lastRequest ?? URLRequest(url: url))
            return
        }
        webView.reload()
    }

    @objc private func tapToFavoritButton() {
        isFavourite = !isFavourite
        self.favouriteBarButton?.image = isFavourite ?  #imageLiteral(resourceName: "FavouriteSelect") : #imageLiteral(resourceName: "FavouriteUnselect")
    }
}
