//
//  OptionsTableViewController.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 19/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import Foundation
import UIKit

class OptionsTableViewController: UITableViewController {

    deinit {
        delegate?.finish(hasChanges: hasChanges || Options.hasChanges)
    }

    private let moc = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    private var hasChanges = false
    weak var delegate: BackToFeedsListViewDelegate?

    private enum OptionFields {
        case onlyFavorites
        case grupByChannel
        case deleteAllFeeds
    }

    private let fields: [OptionFields] = [.onlyFavorites, .grupByChannel, .deleteAllFeeds]

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fields.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCellID", for: indexPath)
        guard let optionCell = cell as? OptionsTableViewCell else {
            return UITableViewCell()
        }
        switch fields[indexPath.row] {
        case .onlyFavorites:
            optionCell.configure(description: "Only favorites", state: Options.onlyFavorites) { isOn in
                Options.onlyFavorites = isOn
            }
        case .grupByChannel:
            optionCell.configure(description: "Grup by channel", state: Options.grupByChannel) { isOn in
                Options.grupByChannel = isOn
            }
        case .deleteAllFeeds:
            optionCell.configure(description: "Delete all feeds")
            optionCell.backgroundColor = UIColor.red
        }
        return optionCell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch fields[indexPath.row] {
        case .onlyFavorites,
             .grupByChannel:
            tableView.deselectRow(at: indexPath, animated: true)
        case .deleteAllFeeds:
            showDestructiveAlert(message: "Are you sure you want to delete all feeds") { [weak self] in
                guard let moc = self?.moc else {
                    return
                }
                try? Feed.cleanAll(in: moc)
                try? moc.save()
                self?.hasChanges = true
            }
        }
    }
}
