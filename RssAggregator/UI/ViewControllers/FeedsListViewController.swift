//
//  FeedsViewController.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 15/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit
import FeedKit
import CoreData

protocol BackToFeedsListViewDelegate: class {
    func finish(hasChanges: Bool)
}

protocol BackToFeedsListFromFeedViewDelegate: class {
    func feedViewWillClose(isFavourite: Bool, indexPath: IndexPath?)
}

class FeedsListViewController: UITableViewController {

    private let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    private var moc: NSManagedObjectContext? {
        return container?.viewContext
    }
    private var channels: [Channel] = []
    private var feeds: [[Feed]] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        addSomeChannels()
        tableView.addPullToRefresh(target: self, action: #selector(refresh(sender:)))
        loadCoreDataFeeds()
        return
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "OpenChannelsList":
            if let fivc = segue.destination as? ChannelsListViewController {
                fivc.delegate = self
            }
        case "OpenFeedID":
            guard let indexPath = self.tableView.indexPathForSelectedRow,
                let fivc = segue.destination as? FeedViewController else {
                return
            }
            let feed = feeds[indexPath.section][indexPath.row]
            fivc.delegate = self
            fivc.configure(with: feed.url, isFavourite: feed.isFavourite, indexPath: indexPath)
        case "OpenOptionsID":
            if let fivc = segue.destination as? OptionsTableViewController {
                fivc.delegate = self
            }
        default:
            break
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Options.grupByChannel ? channels.count : 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedListTableViewCellID", for: indexPath)
        guard let feedCell = cell as? FeedsListTableViewCell else {
            return UITableViewCell()
        }
        let feed = feeds[indexPath.section][indexPath.row]
        let icon = feed.img() ?? #imageLiteral(resourceName: "Placeholder")
        let channelIcon = feed.channel?.img() ?? #imageLiteral(resourceName: "Placeholder")
        feedCell.configure(icon: icon, title: feed.title, description: feed.descriptionText,
                           pubDate: feed.pubDate?.toString, channelIcon: channelIcon,
                           isFavourite: feed.isFavourite) { [weak self] in
            guard let indexPath = self?.tableView.indexPath(for: feedCell) else {
                return
            }
            self?.feeds[indexPath.section][indexPath.row].isFavourite.inverse()
            try? self?.moc?.save()
            if Options.onlyFavorites {
                self?.feeds[indexPath.section].remove(at: indexPath.row)
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
        return feedCell
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Options.grupByChannel ? 70.0 : 0
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !Options.grupByChannel {
            return nil
        }
        let cell = Bundle.main.loadNibNamed("FeedsListTableViewHeader", owner: self, options: nil)?.first
        guard let header = cell as? FeedsListTableViewHeader  else {
            return nil
        }
        let icon = channels[section].img() ?? #imageLiteral(resourceName: "Placeholder")
        let name = channels[section].name
        let title = channels[section].title
        let subtitle = "Last update at: " + (channels[section].lastUpdateAt?.toString ?? "No RSS feeds found")
        header.configure(icon: icon, name: name, title: title, subtitle: subtitle)
        return header
    }

    // MARK: - Private

    private func loadCoreDataFeeds() {
        guard let moc = moc else {
            return
        }
        moc.refreshAllObjects()
        channels = (try? Channel.getVisible(in: moc)) ?? []
        let allFetchedFeeds = (try? Feed.getFeedsAccordingToOptions(in: moc)) ?? []
        if Options.grupByChannel {
            channels.forEach { channel in
                let feedsForChannel = allFetchedFeeds.filter({ $0.channel?.url == channel.url })
                feeds.append(feedsForChannel)
            }
        } else {
            feeds.append(allFetchedFeeds)
        }
    }

    @objc private func refresh(sender: UIRefreshControl) {
        if let container = container {
            FeedManager.shared.loadFeedsFromInternet(to: container) { [weak self] _ in
                self?.reloadData()
            }
        }
    }

    private func reloadData() {
        channels = []
        feeds = []
        loadCoreDataFeeds()
        tableView.reloadData()
        tableView.refreshControl?.endRefreshing()
    }

    // TODO: move to test
    private func addSomeChannels() {
        let name = ["Pikabu", "NYT", "Habr"]
        let urlStrings = ["https://pikabu.ru/xmlfeeds.php?cmd=popular",
                          "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml",
                          "https://habr.com/ru/rss/all/all/"]
        guard let moc = moc, let channels = try? Channel.getAll(in: moc) else {
            return
        }
        for index in 0..<urlStrings.count {
            if !channels.contains(where: { $0.url == urlStrings[index]}) {
                let newChannel = Channel(context: moc)
                newChannel.setProperties(name: name[index], url: urlStrings[index], addedAt: Date())
            }
        }
        try? moc.save()
    }
}

extension FeedsListViewController: BackToFeedsListViewDelegate {
    func finish(hasChanges: Bool) {
        if hasChanges {
            reloadData()
        }
    }
}

extension FeedsListViewController: BackToFeedsListFromFeedViewDelegate {
    func feedViewWillClose(isFavourite: Bool, indexPath: IndexPath?) {
        guard let indexPath = indexPath else { return }
        feeds[indexPath.section][indexPath.row].isFavourite = isFavourite
        try? moc?.save()
        if Options.onlyFavorites {
            feeds[indexPath.section].remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
}
