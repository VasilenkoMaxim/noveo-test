//
//  AddChannelViewController.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 16/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit

protocol AddChannelViewControllerDelegate: class {
    func finish(name: String, url: String, date: Date, oldUrl: String?) throws
}

class AddChannelViewController: UIViewController {

    weak var delegate: AddChannelViewControllerDelegate?
    private var oldUrl: String?
    private var oldName: String?
    private var isEditViewController: Bool = false

    @IBOutlet weak var nameChannelTextField: UITextField!
    @IBOutlet weak var urlChannelTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        if isEditViewController {
            addButton.setTitle("Save", for: .normal)
            nameChannelTextField.text = oldName
            urlChannelTextField.text = oldUrl
        }
    }

    @IBAction func add(_ sender: Any) {
        guard let name = nameChannelTextField.text, let url = urlChannelTextField.text else {
                showErrorAlert(message: "Something went wrong")
            return
        }
        guard !url.isEmpty else {
            showErrorAlert(message: ChannelError.urlNotSet.errorDescription)
            return
        }
        guard !name.isEmpty else {
            showErrorAlert(message: ChannelError.nameNotSet.errorDescription)
            return
        }
        do {
            try delegate?.finish(name: name, url: url, date: Date(), oldUrl: oldUrl)
        } catch {
            showErrorAlert(message: (error as? LocalizedError)?.errorDescription ?? "Something went wrong")
            return
        }
        navigationController?.popViewController(animated: true)
    }

    func configureForEdit(name: String, url: String) {
        oldName = name
        oldUrl = url
        isEditViewController = true
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate

extension AddChannelViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameChannelTextField:
            urlChannelTextField.becomeFirstResponder()
        case urlChannelTextField: view.endEditing(true)
        default: break
        }
        return false
    }
}
