//
//  ChannelsViewController.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 15/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit

class ChannelsListViewController: UITableViewController {

    deinit {
        delegate?.finish(hasChanges: hasChanges)
    }

    weak var delegate: BackToFeedsListViewDelegate?
    private var hasChanges = false
    private var channels: [Channel] = []
    private let moc = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        if let moc = moc {
            channels = (try? Channel.getAll(in: moc)) ?? []
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openAddChannel" {
            if let fivc = segue.destination as? AddChannelViewController {
                fivc.delegate = self
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return channels.count
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelTableViewCell", for: indexPath)
            cell.textLabel?.text = "SHOW ALL"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 19.0)
            return cell
        case 1:
            let cell = Bundle.main.loadNibNamed("ChannelsTableViewCell", owner: self, options: nil)?.first
            guard let channelCell = cell as? ChannelsTableViewCell else {
                return UITableViewCell()
            }
            let icon = channels[indexPath.row].img() ?? #imageLiteral(resourceName: "Placeholder")
            channelCell.configure(icon: icon, name: channels[indexPath.row].name, url: channels[indexPath.row].url,
                                  state: channels[indexPath.row].isVisible) { [weak self] isOn in
                self?.channels[indexPath.row].isVisible = isOn
                self?.saveMOC()
            }
            return channelCell
        default:
            return tableView.dequeueReusableCell(withIdentifier: "ChannelTableViewCell", for: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0, indexPath.row == 0 {
            channels.forEach({ $0.isVisible = true })
            saveMOC()
            hasChanges = true
            navigationController?.popViewController(animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard indexPath.section == 1 else { return [] }
        let delete = UITableViewRowAction(style: .destructive, title: "DELETE") { [weak self] _, _ in
            self?.showDestructiveAlert(message: "Are You sure you want to delete the channel?") {
                guard let channel = self?.channels[indexPath.row] else { return }
                self?.channels.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
                self?.moc?.delete(channel)
                self?.saveMOC()
            }
        }
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { [weak self] _, _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let customViewController = storyboard.instantiateViewController(withIdentifier: "AddChannelViewController")
            guard let editChannelViewController = customViewController as? AddChannelViewController,
                let name = self?.channels[indexPath.row].name, let url = self?.channels[indexPath.row].url else {
                return
            }
            editChannelViewController.configureForEdit(name: name, url: url)
            editChannelViewController.delegate = self
            self?.navigationController?.pushViewController(editChannelViewController, animated: true)
        }
        return [delete, edit]
    }

    private func saveMOC() {
        guard let moc = moc else { return }
        hasChanges = moc.hasChanges
        try? moc.save()
    }
}

extension ChannelsListViewController: AddChannelViewControllerDelegate {

    func finish(name: String, url: String, date: Date, oldUrl: String? = nil) throws {
        guard let oldUrl = oldUrl else {
            try addChannel(name: name, url: url, date: date)
            return
        }
        try editChannel(with: oldUrl, name: name, url: url, date: date)
    }

    private func addChannel(name: String, url: String, date: Date) throws {
        if channels.contains(where: { return $0.url == url }) {
            throw ChannelError.channelExist
        }
        guard let moc = moc else { return }
        let channel = Channel(context: moc)
        channel.setProperties(name: name, url: url, addedAt: date)
        channels.insert(channel, at: 0)
        tableView.insertRows(at: [IndexPath(row: 0, section: 1)], with: .top)
        moc.insert(channel)
        saveMOC()
    }

    private func editChannel(with oldUrl: String, name: String, url: String, date: Date) throws {
        guard let index = channels.firstIndex(where: { $0.url == oldUrl }) else { return }
        let channel = channels[index]
        if oldUrl == url {
            channel.name = name
            tableView.reloadRows(at: [IndexPath(row: index, section: 1)], with: .automatic)
            saveMOC()
            return
        }
        channels.remove(at: index)
        tableView.deleteRows(at: [IndexPath(row: index, section: 1)], with: .automatic)
        moc?.delete(channel)
        try addChannel(name: name, url: url, date: date)
    }
}
