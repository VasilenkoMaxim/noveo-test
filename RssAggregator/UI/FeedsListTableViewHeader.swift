//
//  FeedsListTableViewHeader.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 18/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit

class FeedsListTableViewHeader: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    func configure(icon: UIImage? = nil, name: String? = nil, title: String? = nil, subtitle: String? = nil) {
        iconView.image = icon
        nameLabel.text = name
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}
