//
//  ChannelsTableViewCell.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 18/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

import UIKit

class ChannelsTableViewCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var switcher: UISwitch!

    private var action: ((_ isOn: Bool) -> Void)?

    @IBAction func switcherChangeValue(_ sender: UISwitch) {
        action?(sender.isOn)
    }

    func configure(icon: UIImage? = nil, name: String? = nil, url: String? = nil,
                   state: Bool? = nil, action: ((Bool) -> Void)? = nil) {
        iconView.image = icon
        nameLabel.text = name
        urlLabel.text = url
        if let state = state {
            switcher.isOn = state
        } else {
            switcher.isHidden = true
        }
        self.action = action
    }
}
