//
//  Options.swift
//  RssAggregator
//
//  Created by Maxim Vasilenko on 19/06/2019.
//  Copyright © 2019 Maxim Vasilenko. All rights reserved.
//

let onlyFavoritesKey = "Options.onlyFavorites"
let grupByChannelKey = "Options.grupByChannel"

import Foundation
import CoreData

struct Options {

    static var onlyFavorites: Bool {
        get {
            return UserDefaults.standard.bool(forKey: onlyFavoritesKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: onlyFavoritesKey)
            hasChanges = true
        }
    }

    static var grupByChannel: Bool {
        get {
            return !UserDefaults.standard.bool(forKey: grupByChannelKey)
        }
        set {
            UserDefaults.standard.set(!newValue, forKey: grupByChannelKey)
            hasChanges = true
        }
    }

    static var hasChanges: Bool = false

    static func predicateForFetchFeed(in moc: NSManagedObjectContext) -> NSPredicate? {
        guard let channels = try? Channel.getVisible(in: moc), !channels.isEmpty else { return nil }
        var format = "\(#keyPath(Feed.channel.url)) == %@"
        for _ in 1..<channels.count {
            format += " OR \(#keyPath(Feed.channel.url)) == %@"
        }
        format = "(" + format + ")"
        format += " AND \(#keyPath(Feed.isFavourite)) == %@"
        var argumentArray: [Any] = channels.map { (channel) -> Any in
            return channel.url ?? ""
        }
        argumentArray.append(onlyFavorites)
        let predicate = NSPredicate(format: format, argumentArray: argumentArray)
        return predicate
    }

    static func sortDescriptorsForFetchFeed() -> [NSSortDescriptor]? {
        var sortDescriptors: [NSSortDescriptor] = []
        if grupByChannel {
            sortDescriptors.append(NSSortDescriptor(key: #keyPath(Feed.channel.addedAt), ascending: false))
        }
        sortDescriptors.append(NSSortDescriptor(key: #keyPath(Feed.pubDate), ascending: false))
        return sortDescriptors
    }
}
